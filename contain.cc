#include "contain.h"
// contain.h

using namespace std;

Contain::Contain(){
    used = 0;
    capacity = 5;
    try{
        data = new Date[capacity];
    }
    catch(bad_alloc){
          cout << "No memory\n";
    }

}

void Contain::resize(){
    try{
    Date * tmp;
    tmp = new Date[capacity + 5];
    copy(data, data + used, tmp);
    capacity += 5;
    delete [] data;
    data = tmp;
    }
    catch(bad_alloc){
          cout << "No memory\n";
    }

}

Contain::~Contain(){
    delete[] data;
}

Contain::Contain(const Contain& other){
    data = new Date[other.capacity];
    capacity = other.capacity;
    used = other.used;
    copy(other.data, other.data + used, data);
}

void Contain::operator =(const Contain& other){
    delete [] data;
    capacity = other.capacity;
    used = other.used;
    data = new Date[capacity];
    copy(other.data, other.data + used, data);


}

void Contain::save(ostream& out){
    for(int i = 0; i < used; i++){
        if(i == used -1){
            out << data[i];
        }
        else{
            out << data[i] << endl;
        }
    }
}

void Contain::start(){
    current_index = 0;
}

void Contain::advance(){
    current_index++;
}

bool Contain::is_item(){
    return(current_index < used);
}

Date Contain::current(){
    return(data[current_index]);
}

void Contain::insert(const Date& temp){
    if(used == capacity-1){
        resize();
    }
    if(used == 0){
        data[0] = temp;
    }
    else{
        for(int i = used; i > current_index; i--){
            data[i] = data[i-1];
        }
    }
    data[current_index] = temp;
    used++;
}

void Contain::remove_current(){
    for(int i = current_index; i < used-1; i++){
        data[i] = data[i+1];
    }
    used--;
}

