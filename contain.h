//*******************************************************************
//
//  Program:     project 2 (first version)
//
//  Author:      Kip Bodey
//
//  Email:       kb669314@ohio.edu
//
//  Description: creates a collection of friends and their birthdays. Uses a pointer
//                and container
//
//  Date:        7/10/15
//  contain.h
//*******************************************************************
//*******************************************************************
/*!

Contain();
Purpose: constructor
Precondition: none
Postcondition: sets used=0 capacity=5 and makes new object

void resize();
Purpose: to increase the size of the array
Precondition: none
Postcondition: increases capacity by 5

~Contain();
Purpose: deconstructor
Precondition: one is constructed
Postcondition: it deallocates memory

Contain(const Contain& other);
Purpose: copy
Precondition: none
Postcondition: copys to another object and changes pointer

void operator =(const Contain& other);
Purpose: makes another equal object
Precondition: two objects are present
Postcondition: sets one object equal to the other

int current_index = 0;
Purpose: index to be used from main
Precondition: none
Postcondition: sets current_index=0

void start();
Purpose: to loop through array in main
Precondition: none
Postcondition: sets current index to zero

void advance();
Purpose: to go through loop
Precondition: none
Postcondition: adds one to current index

bool is_item();
Purpose: is there a next item
Precondition: none
Postcondition: true if another item exists

Date current();
Purpose: to get the current item from the array
Precondition: current index is set to retrieval point
Postcondition: returns the item in the array at that spot

void insert(const Date& temp);
Purpose: to add an item to the array
Precondition: a current index is read in on where to insert
Postcondition: an item will be placed in the array in the correct order by date

void remove_current();
Purpose: remove an item in the array
Precondition: current index is read in
Postcondition: it moves the array to the left one position starting at current index

void save(std::ostream& out);
Purpose: to save to a file
Precondition: ofstream passed in
Postcondition: writes data to a file

Date * data;
Purpose: is the pointer
Precondition: none
Postcondition: makes pointer

size_t used;
Purpose: the amount of things in array

size_t capacity;
Purpose: the size of the total spots available in the array

*/
//*************************************************************************

#include "date.h"
#include <iostream>
#include <algorithm>
class Contain{
public:
    Contain();
    void resize();
    ~Contain();
    Contain(const Contain& other);
    void operator =(const Contain& other);
    int current_index;
    void start();
    void advance();
    bool is_item();
    Date current();
    void insert(const Date& temp);
    void remove_current();
    void save(std::ostream& out);
private:
    Date * data;
    size_t used;
    size_t capacity;

};
