//*******************************************************************
//
//  Program:     project 2 (first version)
//
//  Author:      Kip Bodey
//
//  Email:       kb669314@ohio.edu
//
//  Description: creates a collection of friends and their birthdays. Uses a pointer
//                and container
//
//  Date:        7/10/15
//
//*******************************************************************
//main.cc
#include "date.h"
#include "contain.h"
#include <cstdlib>
#include <fstream>

using namespace std;

int menu();
void open(ifstream& fin, Date& d, Contain& c);
void save(Contain& c);

int main(){

    Date d;
    Contain c;
    Contain copy;
    ifstream fin("data.dat");
    if(fin.fail()){
        cout << "Error loading file!\n";
    }
    else{
        open(fin, d, c);
    }
    fin.close();

    while(true){
        int choice;
        choice = menu();
        switch(choice){
        case 1:
            cout << "Enter date in mm/dd/yyyy format\n";
            cin >> d;
            for(c.start(); d<c.current(); c.advance()){
            }
            c.insert(d);
            break;
        case 2:
            for(c.start(); c.is_item(); c.advance()){
                cout << c.current() << endl;
            }
            break;
        case 3:
            cout << "Enter date in mm/dd/yyyy format\n";
            d.toggle_name_on();
            cin >> d;
            for(c.start(); c.is_item(); c.advance()){
               if(d == c.current()){
                   cout << c.current() << endl;
               }
            }
            d.toggle_name_on();
            break;
        case 4:
            cout << "Enter date in mm/dd/yyyy format\n";
            d.toggle_name_on();
            cin >> d;
            for(c.start(); c.is_item(); c.advance()){
               if(d == c.current()){
                   c.remove_current();
               }
            }
            d.toggle_name_on();
            break;
        case 5:
            copy = c;
            cout << "##### ORIGINAL: #####\n";
            for(c.start(); c.is_item(); c.advance()){
                cout << c.current() << endl;
            }
            cout << "####### COPY: #######\n";

            for(copy.start(); copy.is_item(); copy.advance()){
                cout << copy.current() << endl;
            }
            break;
        case 6:
            save(c);
            return(EXIT_SUCCESS);
            break;
        }
    }
    return 0;
}

int menu(){
    int temp = 0;
    while(temp < 1 || temp > 6){
        cout << "1. Input date and name\n"
             << "2. See all birthdays\n"
             << "3. Search particular date\n"
             << "4. Remove particular date\n"
             << "5. Test copy\n"
             << "6. Save and exit\n";
        cin >> temp;
    }
    return(temp);
}

void open(ifstream& fin, Date& d, Contain& c){
    string temp;
    while(!fin.eof()){
        fin >> d;
        for(c.start(); d<c.current(); c.advance()){
        }
        c.insert(d);
    }
}

void save(Contain& c){
    ofstream out("data.dat");
    c.save(out);
    out.close();
}

