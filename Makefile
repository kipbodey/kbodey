all: main
	make compile
	make doc
	make softclean

compile:
main: main.o date.o contain.o
	@g++ -o main main.o date.o contain.o

main.o: main.cc
	@g++ -c main.cc

date.o: date.cc date.h
	@g++ -c date.cc

contain.o: contain.cc contain.h
	@g++ -c contain.cc

doc:
	@doxygen -g
	@doxygen Doxyfile
	#@rm -rf html
	@cd latex; make; cp refman.pdf ..; make clean;

softclean:
	-rm *.o	
 
clean:
	-rm *.o
	-rm main
	rm -rf latex
	-rm Doxyfile
	-rm refman.pdf

